FROM jenkins/jenkins:lts

USER root

ENV DOCKER_DIR="/home/jenkins/"

RUN apt-get update && \
    apt-get install -y sudo cmake python3 default-jre && \
    apt-get install -y python3-requests && \
    apt install -y gcc g++ 

COPY Jenkinsfile $DOCKER_DIR
COPY CMakeLists.txt $DOCKER_DIR
COPY send_telegram_report.py $DOCKER_DIR
COPY README.md $DOCKER_DIR
COPY main.cpp $DOCKER_DIR

USER jenkins

CMD ["java", "-jar", "/usr/share/jenkins/jenkins.war"]
